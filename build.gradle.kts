plugins {
    application
    java
    id("com.diffplug.spotless") version "6.6.1"
    id("com.github.spotbugs") version "5.0.7"
    id("org.openjfx.javafxplugin") version "0.0.13"
}

repositories {
    mavenCentral()
}

group = "com.scotthamper"
version = "1.0-SNAPSHOT"

application {
    mainClass.set("com.scotthamper.quadtree.ui.Main")
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(16))
    }
}

javafx {
    version = "18.0.1"
    modules = listOf("javafx.controls", "javafx.fxml")
}

spotless {
    java {
        googleJavaFormat().reflowLongStrings()
    }
}

tasks.withType<AbstractArchiveTask>().configureEach {
    // These two settings allow for reproducible JAR builds. See:
    // https://docs.gradle.org/current/userguide/working_with_files.html#sec:reproducible_archives
    isPreserveFileTimestamps = false
    isReproducibleFileOrder = true
}

dependencies {
    spotbugsPlugins("com.h3xstream.findsecbugs:findsecbugs-plugin:1.12.0")
}
