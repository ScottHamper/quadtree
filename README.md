Quadtree Demo
=============

As the number of collidable objects in an environment increases, naive 
collision checks between the objects (where each object tests against each 
other object) inevitably incurs too much of a performance penalty to be 
practical. [Quadtrees](https://en.wikipedia.org/wiki/Quadtree) can alleviate 
this issue by dramatically reducing the number of collision checks required 
each frame.

![A screenshot of the quadtree demo application](screenshot.png)

Controls
--------

- 1 - Disable collision checks (set by default)
- 2 - Enable naive collision checks (each object tests against each 
  other object)
- 3 - Enable quadtree-assisted collision checks

Parameters
----------

The following static constants exist in the application and can be modified 
to change the characteristics of the simulation.

### `model.Quadtree`
- `MAX_DEPTH`: The maximum number of times that the tree will recursively 
  split a region into quadrants, plus one for the initial region (default: 
  `Integer.MAX_VALUE`).
- `MAX_SHAPES_PER_NODE`: The maximum number of shapes that can be stored in 
  a tree node before the node's region is split into smaller quadrants. Note 
  that a node may still end up containing more than `MAX_SHAPES_PER_NODE` 
  shapes. For example, if the `MAX_DEPTH` has been reached and a shape would 
  be placed in a bottom-most node of the tree, or if a shape cannot be fully 
  contained in one of a node's sub-quadrants (default: `1`).

### `model.World`
- `CIRCLE_MIN_RADIUS`: The inclusive lower bound for each circle's randomly 
  chosen radius (default: `0.05` meters).
- `CIRCLE_MAX_RADIUS`: The exclusive upper bound for each circle's randomly 
  chosen radius (default: `0.3` meters).
- `CIRCLE_MIN_SPEED`: The inclusive lower bound for each circle's randomly 
  chosen speed (default: `0.25` meters per second).
- `CIRCLE_MAX_SPEED`: The exclusive upper bound for each circle's randomly 
  chosen speed (default: `3.0` meters per second).

### `ui.screen.WorldScreen`
- `WIDTH`: The width of the application window, in pixels (default: `1024`).
- `HEIGHT`: The height of the application window, in pixels (default: `768`).
- `PIXELS_PER_METER`: The number of pixels used to represent a meter. Can
  think of this like a zoom control - smaller values result in the camera
  being farther back, larger values result in the camera being closer
  (default: `10.0`).
- `CIRCLE_COUNT`: The number of circles created in the world. Adjust to see
  what your specific hardware can handle (default: `8000`).