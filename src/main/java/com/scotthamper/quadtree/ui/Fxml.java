package com.scotthamper.quadtree.ui;

import java.io.IOException;
import java.net.URL;
import java.util.MissingResourceException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;

public class Fxml {
  /**
   * Loads an FXML file associated with the specified Parent instance. The FXML file is expected to
   * share the same name as the Parent instance's class. Additionally, if a CSS file also exists
   * with the same name, it will be added to the Parent automatically.
   *
   * @param parent The Parent instance that will become the controller for the loaded FXML file.
   */
  public static void load(Parent parent) {
    String parentName = parent.getClass().getSimpleName();

    var loader = new FXMLLoader(parent.getClass().getResource(parentName + ".fxml"));
    loader.setRoot(parent);
    loader.setController(parent);

    try {
      loader.load();
    } catch (IOException e) {
      // As FXML loading in this method is convention based, it's considered a programming bug
      // if the FXML file doesn't exist. An `IOException` could also occur if the file *does*
      // exist but can't be opened for some reason. However, in that case there's not an
      // obvious way for us to recover, so we'll still let the program crash. The reason we can't
      // simply let the IOException be thrown is because it's a managed exception, and we don't want
      // to force callers to deal with it.
      throw new MissingResourceException(
          "Couldn't load FXML file", parentName, parentName + ".fxml");
    }

    URL cssUrl = parent.getClass().getResource(parentName + ".css");

    if (cssUrl != null) {
      parent.getStylesheets().add(cssUrl.toExternalForm());
    }
  }
}
