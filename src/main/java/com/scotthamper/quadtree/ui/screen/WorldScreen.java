package com.scotthamper.quadtree.ui.screen;

import com.scotthamper.quadtree.model.Rectangle;
import java.util.Set;
import javafx.fxml.FXML;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import com.scotthamper.quadtree.model.Circle;
import com.scotthamper.quadtree.model.CollisionStrategy;
import com.scotthamper.quadtree.model.World;
import com.scotthamper.quadtree.ui.ApplicationTimer;
import com.scotthamper.quadtree.ui.Fxml;

public class WorldScreen extends VBox {
  private static final int WIDTH = 1024;
  private static final int HEIGHT = 768;
  private static final double PIXELS_PER_METER = 10.0;
  private static final int CIRCLE_COUNT = 8000;

  @FXML private Canvas canvas;

  private final World world;

  public WorldScreen() {
    world = new World(WIDTH / PIXELS_PER_METER, HEIGHT / PIXELS_PER_METER, CIRCLE_COUNT);

    Fxml.load(this);
  }

  @FXML
  private void initialize() {
    sceneProperty()
        .addListener((p, o, n) -> n.addEventFilter(KeyEvent.KEY_TYPED, this::onKeyTyped));

    canvas.setWidth(WIDTH);
    canvas.setHeight(HEIGHT);
    new ApplicationTimer(world::update, this::render).start();
  }

  private void render() {
    GraphicsContext context = canvas.getGraphicsContext2D();

    context.setFill(Color.WHITE);
    context.fillRect(0, 0, canvas.getWidth(), canvas.getHeight());

    context.setStroke(Color.gray(0.25));
    context.setFill(Color.rgb(230, 20, 20, .75));

    Set<Circle> collidingCircles = world.collidingCircles();

    for (Circle circle : world.circles()) {
      Rectangle boundingBox = circle.boundingBox();
      double x = boundingBox.left() * PIXELS_PER_METER;

      // Positive Y is up in our model, but down when rendering
      double y = canvas.getHeight() - boundingBox.top() * PIXELS_PER_METER;

      double width = boundingBox.width() * PIXELS_PER_METER;
      double height = boundingBox.height() * PIXELS_PER_METER;

      if (collidingCircles.contains(circle)) {
        context.fillOval(x, y, width, height);
      }

      context.strokeOval(x, y, width, height);
    }
  }

  private void onKeyTyped(KeyEvent event) {
    world.setCollisionStrategy(
        switch (event.getCharacter()) {
          case "1" -> CollisionStrategy.NONE;
          case "2" -> CollisionStrategy.NAIVE;
          case "3" -> CollisionStrategy.quadtree(world.width(), world.height());
          default -> world.collisionStrategy();
        });
  }
}
