package com.scotthamper.quadtree.ui;

// In order to execute an assembled JAR file for the project via the CLI, we must use a `Main` class
// that does not extend the JavaFX `Application` class. However, in order for JavaFX to run, we do
// eventually need to execute a `main` method in a subclass of `javafx.application.Application`.
// Thus, the only purpose of this class is to kick off the JavaFX application. No other code should
// ever be added to this class.
public class Main {
  public static void main(String[] args) {
    JavaFxApplication.main(args);
  }
}
