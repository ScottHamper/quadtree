package com.scotthamper.quadtree.ui;

import java.util.function.Consumer;
import javafx.animation.AnimationTimer;

public class ApplicationTimer extends AnimationTimer {
  private static final int NANOSECONDS_PER_SECOND = 1_000_000_000;
  private static final double UPDATE_TIME_DELTA = 0.01;

  private final Runnable renderFunction;
  private final Consumer<Double> updateFunction;
  private long lastRenderTime;
  private long lastUpdateTime;

  public ApplicationTimer(Consumer<Double> updateFunction, Runnable renderFunction) {
    this.updateFunction = updateFunction;
    this.renderFunction = renderFunction;
  }

  @Override
  public void start() {
    lastRenderTime = System.nanoTime();
    lastUpdateTime = lastRenderTime;
    super.start();
  }

  @Override
  public void handle(long now) {
    lastRenderTime = now;
    renderFunction.run();

    // Using a constant update time delta allows our model updates to be deterministic. We then
    // need to do as many updates that fit within the render time delta as possible so that it
    // still looks like everything is happening in real time.
    while (lastUpdateTime < lastRenderTime) {
      updateFunction.accept(UPDATE_TIME_DELTA);
      lastUpdateTime += UPDATE_TIME_DELTA * NANOSECONDS_PER_SECOND;
    }
  }
}
