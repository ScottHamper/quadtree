package com.scotthamper.quadtree.ui;

import javafx.scene.Scene;
import javafx.stage.Stage;
import com.scotthamper.quadtree.ui.screen.WorldScreen;

public class JavaFxApplication extends javafx.application.Application {
  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage stage) {
    var scene = new Scene(new WorldScreen());

    scene.rootProperty().addListener((p, o, n) -> stage.sizeToScene());
    stage.setTitle("Quadtree");
    stage.setScene(scene);
    stage.show();
  }
}
