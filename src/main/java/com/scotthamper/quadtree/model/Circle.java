package com.scotthamper.quadtree.model;

import java.util.Objects;

public class Circle implements Shape {
  private final double radius;
  private Vector2D position;
  private Vector2D velocity;

  public Circle(double radius, Vector2D position, Vector2D velocity) {
    this.radius = radius;
    this.position = position;
    this.velocity = velocity;
  }

  public double radius() {
    return radius;
  }

  public Vector2D position() {
    return position;
  }

  public void setPosition(Vector2D position) {
    this.position = position;
  }

  public void setPosition(double x, double y) {
    setPosition(new Vector2D(x, y));
  }

  public Vector2D velocity() {
    return velocity;
  }

  public void setVelocity(Vector2D velocity) {
    this.velocity = velocity;
  }

  public void setVelocity(double x, double y) {
    setVelocity(new Vector2D(x, y));
  }

  public boolean isIntersecting(Circle other) {
    Vector2D collisionVector = position.subtract(other.position);

    // It's faster to square the sum of the radii than it is to compute a square root.
    return collisionVector.magnitudeSquared() <= Math.pow(radius + other.radius, 2);
  }

  public void update(double timeDelta) {
    position = position.add(velocity.scale(timeDelta));
  }

  @Override
  public Rectangle boundingBox() {
    return new Rectangle(position.subtract(radius, radius), radius * 2.0, radius * 2.0);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null || getClass() != obj.getClass()) {
      return false;
    }

    final var other = (Circle) obj;

    return Double.compare(other.radius, radius) == 0
        && position.equals(other.position)
        && velocity.equals(other.velocity);
  }

  @Override
  public int hashCode() {
    return Objects.hash(position, velocity, radius);
  }
}
