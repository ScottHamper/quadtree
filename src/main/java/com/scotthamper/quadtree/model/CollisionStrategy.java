package com.scotthamper.quadtree.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public interface CollisionStrategy {
  CollisionStrategy NONE = circles -> Set.of();

  CollisionStrategy NAIVE =
      circles -> {
        Set<Circle> result = new HashSet<>();

        for (int i = 0; i < circles.size() - 1; i++) {
          for (int j = i + 1; j < circles.size(); j++) {
            if (circles.get(i).isIntersecting(circles.get(j))) {
              result.add(circles.get(i));
              result.add(circles.get(j));
            }
          }
        }

        return result;
      };

  static CollisionStrategy quadtree(double width, double height) {
    return circles -> {
      Set<Circle> result = new HashSet<>();
      var quadtree = new Quadtree(width, height);

      for (Circle circle : circles) {
        quadtree.insert(circle);

        for (Shape other : quadtree.getShapesNear(circle)) {
          if (circle != other && circle.isIntersecting((Circle) other)) {
            result.add(circle);
            result.add((Circle) other);
          }
        }
      }

      return result;
    };
  }

  // TODO: Generify to Set<Shape>/List<Shape>
  Set<Circle> collidingCircles(List<Circle> circles);
}
