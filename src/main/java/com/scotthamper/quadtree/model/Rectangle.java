package com.scotthamper.quadtree.model;

public record Rectangle(Vector2D position, double width, double height) implements Shape {
  public double left() {
    return position.x();
  }

  public double right() {
    return position.x() + width;
  }

  public double top() {
    return position.y() + height;
  }

  public double bottom() {
    return position.y();
  }

  public boolean isIntersecting(Rectangle other) {
    return left() <= other.right()
        && right() >= other.left()
        && top() >= other.bottom()
        && bottom() <= other.top();
  }

  public boolean contains(Rectangle other) {
    return left() <= other.left()
        && right() >= other.right()
        && top() >= other.top()
        && bottom() <= other.bottom();
  }

  public Rectangle boundingBox() {
    return this;
  }
}
