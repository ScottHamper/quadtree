package com.scotthamper.quadtree.model;

import java.util.ArrayList;
import java.util.List;

public class Quadtree {
  private static final int MAX_DEPTH = Integer.MAX_VALUE;
  private static final int MAX_SHAPES_PER_NODE = 1;

  private final Node root;
  private int size;

  public Quadtree(final double width, final double height) {
    this.root = new Node(1, new Rectangle(Vector2D.ZERO, width, height));
  }

  public void insert(final Shape shape) {
    root.insert(shape);
    size++;
  }

  public Iterable<Shape> getShapesNear(Shape shape) {
    List<Shape> result = new ArrayList<>(size);

    root.addShapesNear(shape, result);

    return result;
  }

  private static final class Node {
    private final int depth;
    private final Rectangle boundingBox;
    private final List<Shape> shapes;
    private final Node[] quadrants;

    Node(int depth, Rectangle boundingBox) {
      this.boundingBox = boundingBox;
      this.depth = depth;
      this.shapes = new ArrayList<>(MAX_SHAPES_PER_NODE);
      quadrants = new Node[4];
    }

    private boolean isLeafNode() {
      // All quadrants will be null if this node is a leaf node, but we'll just check one.
      return quadrants[0] == null;
    }

    private void split() {
      if (!isLeafNode()) {
        return;
      }

      Vector2D origin = boundingBox.position();
      double width = boundingBox.width() / 2.0;
      double height = boundingBox.height() / 2.0;

      Rectangle southWestBox = new Rectangle(origin, width, height);
      Rectangle southEastBox = new Rectangle(origin.add(width, 0), width, height);
      Rectangle northEastBox = new Rectangle(origin.add(width, height), width, height);
      Rectangle northWestBox = new Rectangle(origin.add(0, height), width, height);

      quadrants[0] = new Node(depth + 1, southWestBox);
      quadrants[1] = new Node(depth + 1, southEastBox);
      quadrants[2] = new Node(depth + 1, northEastBox);
      quadrants[3] = new Node(depth + 1, northWestBox);
    }

    private void insert(Shape shape) {
      if (depth >= MAX_DEPTH || shapes.size() < MAX_SHAPES_PER_NODE) {
        shapes.add(shape);
        return;
      }

      split();

      // This strategy has the side effect of allowing shapes that were inserted into the node
      // earlier to remain in the node, even if they could/should be pushed into a quadrant
      // after the node was subsequently split.
      for (Node quadrant : quadrants) {
        if (quadrant.boundingBox.contains(shape.boundingBox())) {
          quadrant.insert(shape);
          return;
        }
      }

      shapes.add(shape);
    }

    private void addShapesNear(Shape shape, List<Shape> list) {
      if (!boundingBox.isIntersecting(shape.boundingBox())) {
        return;
      }

      list.addAll(shapes);

      if (isLeafNode()) {
        return;
      }

      for (Node quadrant : quadrants) {
        quadrant.addShapesNear(shape, list);
      }
    }
  }
}
