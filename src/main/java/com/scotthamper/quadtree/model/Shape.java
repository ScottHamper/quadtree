package com.scotthamper.quadtree.model;

public interface Shape {
  Rectangle boundingBox();
}
