package com.scotthamper.quadtree.model;

public record Vector2D(double x, double y) {
  public static final Vector2D ZERO = new Vector2D(0.0, 0.0);

  public Vector2D add(Vector2D other) {
    return add(other.x, other.y);
  }

  public Vector2D add(double x, double y) {
    return new Vector2D(this.x + x, this.y + y);
  }

  public Vector2D subtract(Vector2D other) {
    return subtract(other.x, other.y);
  }

  public Vector2D subtract(double x, double y) {
    return new Vector2D(this.x - x, this.y - y);
  }

  public Vector2D scale(double multiplier) {
    return new Vector2D(x * multiplier, y * multiplier);
  }

  public double magnitudeSquared() {
    return x * x + y * y;
  }
}
