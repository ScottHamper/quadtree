package com.scotthamper.quadtree.model;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.function.BiFunction;

public class World {
  // FIXME?: These values are only used when creating/adding random circles to the world. RNG for
  //  circles is scoped to the constructor, so perhaps these should be, as well.
  private static final double CIRCLE_MIN_RADIUS = 0.05; // meters
  private static final double CIRCLE_MAX_RADIUS = 0.3; // meters
  private static final double CIRCLE_MIN_SPEED = 0.25; // meters/sec
  private static final double CIRCLE_MAX_SPEED = 3.0; // meters/sec

  private final double width; // meters
  private final double height; // meters
  private final List<Circle> circles;
  private CollisionStrategy collisionStrategy;

  public World(double width, double height, int circleCount) {
    this.width = width;
    this.height = height;
    circles = new ArrayList<>(circleCount);
    collisionStrategy = CollisionStrategy.NONE;

    var rng = new SecureRandom();

    BiFunction<Double, Double, Double> randomDouble =
        (min, max) -> rng.nextDouble() * (max - min) + min;

    for (int i = 0; i < circleCount; i++) {
      double radius = randomDouble.apply(CIRCLE_MIN_RADIUS, CIRCLE_MAX_RADIUS);
      double x = randomDouble.apply(radius, width - radius);
      double y = randomDouble.apply(radius, height - radius);
      double speedX = randomDouble.apply(CIRCLE_MIN_SPEED, CIRCLE_MAX_SPEED);
      double speedY = randomDouble.apply(CIRCLE_MIN_SPEED, CIRCLE_MAX_SPEED);

      var position = new Vector2D(x, y);

      var velocity =
          new Vector2D(rng.nextBoolean() ? speedX : -speedX, rng.nextBoolean() ? speedY : -speedY);

      circles.add(new Circle(radius, position, velocity));
    }
  }

  public double width() {
    return width;
  }

  public double height() {
    return height;
  }

  public List<Circle> circles() {
    return Collections.unmodifiableList(circles);
  }

  public CollisionStrategy collisionStrategy() {
    return collisionStrategy;
  }

  public void setCollisionStrategy(CollisionStrategy collisionStrategy) {
    this.collisionStrategy = collisionStrategy;
  }

  public void update(double timeDelta) {
    for (Circle circle : circles) {
      circle.update(timeDelta);

      if (circle.position().x() <= circle.radius()) {
        circle.setPosition(circle.radius(), circle.position().y());
        circle.setVelocity(circle.velocity().x() * -1, circle.velocity().y());
      } else if (circle.position().x() >= width - circle.radius()) {
        circle.setPosition(width - circle.radius(), circle.position().y());
        circle.setVelocity(circle.velocity().x() * -1, circle.velocity().y());
      }

      if (circle.position().y() <= circle.radius()) {
        circle.setPosition(circle.position().x(), circle.radius());
        circle.setVelocity(circle.velocity().x(), circle.velocity().y() * -1);
      } else if (circle.position().y() >= height - circle.radius()) {
        circle.setPosition(circle.position().x(), height - circle.radius());
        circle.setVelocity(circle.velocity().x(), circle.velocity().y() * -1);
      }
    }
  }

  public Set<Circle> collidingCircles() {
    return collisionStrategy.collidingCircles(circles);
  }
}
